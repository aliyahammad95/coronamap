# Coronamap

This project aims to give an overview of the corona cases data in Germany, provided by the Robert-Koch Institut. The data was analyzed using R and visualized on a Shinyapp. The app is made interactive using the Leaflet library. 

# Setup

- Install docker
- Build the docker image
- Run docker image
- Navigate to http://localhost:3838/

# How the map works
 
 When running the app, it will take some time till the map is loaded please be patient.

![Alt text](examples/Bildschirmfoto von 2021-01-31 23-37-04.png)

The initial map is of germany and has all 16 states colored in different colors. When hovering on a state with the mouse it shows the total number ofcases in this state, the number of deaths, and the number of recovered cases.

![Alt text](examples/croppes.png)

When clicking on a state two plots are shown.  one shows the cases by genderand the other by age.  If no plots are shown that means no data exists for thisdate, so please move the slider above the map till the plots appear.  Afterclicking the map is zoomed in to show the state and the districts are alsoshown.  When hovering on the districts, you can see their names.

![Alt text](examples/cropped2.png)

Clicking on a district also changes the plots, but you might have to play withthe dates to get results.  Clicking outside a map reurns the initial map of thestates.  If you click on another state the map is zoomed in to the other stateand shows its districts.
