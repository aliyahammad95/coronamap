---
title: "Data Preperation"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Loading the data

We first have to load the data from the csv file, please change the path to match yours. 


```{r}
data <- read.csv("RKI_COVID19.csv", 
               header = TRUE, 
               quote="\"", 
               stringsAsFactors= TRUE, 
               strip.white = TRUE) 
```

# Summary 1

First, we show a summary of the number of cases grouped by gender

```{r}
data_by_gender <- dplyr::group_by(data, Geschlecht)
numcases_by_gender <- dplyr::summarise(data_by_gender, Anzahl = sum(AnzahlFall))

summary(numcases_by_gender)
```


# Summary 2


Next, we show a summary of the number of cases grouped by age

```{r}
data_by_age <- dplyr::group_by(data, Altersgruppe)
numcases_by_age <- dplyr::summarise(data_by_age, Anzahl = sum(AnzahlFall))

summary(numcases_by_age)
```

# Plot 1

In this plot we show a bar diagram with the number of cases grouped by gender. 

```{r}
data_by_gender <- dplyr::group_by(data, Geschlecht)
numcases_by_gender <- dplyr::summarise(data_by_gender, Anzahl = sum(AnzahlFall))

library(ggplot2)
ggplot(data = numcases_by_gender,
       mapping = aes(x = Geschlecht, y = Anzahl)) +
  geom_bar(stat='identity', fill = "indianred3", 
           color = "black")

```


# Plot 2

In this plot we show a pie chart with the number of cases grouped by age group. 

```{r}
data_by_age <- dplyr::group_by(data, Altersgruppe)
numcases_by_age<- dplyr::summarize(data_by_age, Anzahl = sum(AnzahlFall))

library(ggplot2)
 ggplot(numcases_by_age, 
         aes(x = "", 
             y = Anzahl, 
             fill = Altersgruppe)) +
    geom_bar(width = 1, 
             stat = "identity", 
             color = "black") +
    coord_polar("y", 
                start = 0, 
                direction = -1) +
    theme_void()

```

# Plot 3

In this plot we show a tree map with the number of cases grouped by state. 

```{r}
data_by_state <- dplyr::group_by(data, Bundesland)
numcases_by_state<- dplyr::summarize(data_by_state, Anzahl = sum(AnzahlFall))

library(treemapify)
library(ggplot2)
 ggplot(numcases_by_state, 
       aes(fill = Bundesland, 
           area = Anzahl)) +
  geom_treemap() + 
  labs(title = "Number of cases by state")

```


# Plot 4

In this plot we show a tree map with the number of cases grouped by registeration date. 

```{r}
data_by_date <- dplyr::group_by(data, Meldedatum)
numcases_by_date <- dplyr::summarise(data_by_date, Anzahl = sum(AnzahlFall))

library(ggplot2)
ggplot(data = numcases_by_date,
       mapping = aes(x = Meldedatum, 
                     y = Anzahl,
                     )) +
  geom_point()
```


# Plot 5

In this plot we show a pie chart with the number of deaths grouped by age. 

```{r}
data_by_age <- dplyr::group_by(data, Altersgruppe)
numdeaths_by_age <- dplyr::summarise(data_by_age, Anzahl = sum(AnzahlTodesfall))

library(ggplot2)
 ggplot(numdeaths_by_age, 
         aes(x = "", 
             y = Anzahl, 
             fill = Altersgruppe)) +
    geom_bar(width = 1, 
             stat = "identity", 
             color = "black") +
    coord_polar("y", 
                start = 0, 
                direction = -1) +
    theme_void()
```

# Cleaning data for the corona map app

Next, we delete the rows that have a missing value on any of the variables: IdBundesland, Bundesland, IdLandkreis, Landkreis, Altersgruppe, Geschlecht, AnzahlFall, AnzahlTodesfall, Meldedatum.


```{r}

clean_data <- dplyr::select(data, IdBundesland, Bundesland, IdLandkreis, Landkreis, Altersgruppe, Geschlecht, AnzahlFall, AnzahlTodesfall, Meldedatum)
clean_data <- na.omit(clean_data)

```



